<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_for_Claim_Escalation</fullName>
        <description>Email for Claim Escalation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Claim_Escalation</template>
    </alerts>
    <alerts>
        <fullName>Kaiser_New_Case_Email_Notification</fullName>
        <ccEmails>olufemiokunowo@yahoo.com</ccEmails>
        <description>Kaiser New Case Email Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Contact_Route_To_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Kaiser_Case</template>
    </alerts>
    <alerts>
        <fullName>X1_hour_warning_before_milestone_expires</fullName>
        <description>1 hour warning before milestone expires</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/One_hour_warning_before_milestone_expires</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Contact_Route_To_Email</fullName>
        <field>Contact_Route_To_Email__c</field>
        <formula>Contact.Routed_To_Email__c</formula>
        <name>Update Contact Route To Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Is_Escalated_Field</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>Updated Is Escalated Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_Qualtrics_Survey</fullName>
        <apiVersion>40.0</apiVersion>
        <description>The workflow rules send qualtrics survey when case status is &quot;Case Closed&quot;</description>
        <endpointUrl>https://salesforcetrial.qualtrics.com/WRQualtricsServer/sfApi.php?r=outboundMessage&amp;u=UR_dcCVEFRd2JLkFdr&amp;s=SV_42BzFVjOI5i66ln&amp;t=TR_cuKsywLrX1jlCq9</endpointUrl>
        <fields>CaseNumber</fields>
        <fields>Id</fields>
        <fields>Status</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>femiokunowo@gmail.com</integrationUser>
        <name>Send Qualtrics Survey</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Qualtrics Survey Demo</fullName>
        <actions>
            <name>Send_Qualtrics_Survey</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Case Closed</value>
        </criteriaItems>
        <description>Workflow rule to send survey when case status is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Kaiser New Case Notification</fullName>
        <actions>
            <name>Kaiser_New_Case_Email_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Contact_Route_To_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
