<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Idea_Content_Has_Been_Modified</fullName>
        <description>Idea Content Has Been Modified</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Idea_Updated_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Idea_Submitted_For_Revieiw</fullName>
        <ccEmails>olufemiokunowo@yahoo.com</ccEmails>
        <description>New Idea Submitted For Revieiw</description>
        <protected>false</protected>
        <recipients>
            <recipient>femiokunowo@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/New_Idea_For_Review</template>
    </alerts>
    <alerts>
        <fullName>Send_New_Idea_Fro_Review_Email</fullName>
        <ccEmails>olufemiokunowo@yahoo.com</ccEmails>
        <description>Send New Idea Fro Review Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>femiokunowo@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/New_Idea_For_Review</template>
    </alerts>
    <rules>
        <fullName>Change Record Type For SSI Idea</fullName>
        <active>false</active>
        <formula>ISNEW() &amp;&amp;  ISPICKVAL(SSI_Marking__c , &apos;Yes&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Submit New Idea Fro Review</fullName>
        <actions>
            <name>Send_New_Idea_Fro_Review_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
