<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>GEPS_PLD_File_Upload</fullName>
        <description>GEPS PLD File Upload</description>
        <protected>false</protected>
        <recipients>
            <recipient>femiokunowo@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rebekahtotten.recruiter@yahoo.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/GEPS_PLD_File_Email</template>
    </alerts>
    <rules>
        <fullName>GEPS PLD File Upload</fullName>
        <actions>
            <name>GEPS_PLD_File_Upload</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>if ( User_Flag__c , false, 
(ISCHANGED(  IsPLDFileUploaded__c ) &amp;&amp; 
PRIORVALUE(IsPLDFileUploaded__c ) = false &amp;&amp; 
ISPICKVAL( Pricing_Category__c  , &apos;GEPS Custom&apos;)
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
