<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Final_Position_Approval_Email_Notification</fullName>
        <description>Final Position Approval Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>All_Interviewers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Final_Step_Email_For_New_Position_Approval</template>
    </alerts>
    <alerts>
        <fullName>New_Position_Approval_Email_Notification</fullName>
        <description>New Position Approval Email Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>femiokunowo@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>janet.robinson@yahoo.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approve_New_Positions</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_by_Manager</fullName>
        <field>Substatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved by Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_New_Position_To_Recruiter_Queue</fullName>
        <description>This would assign the new position to Recruiter Queue</description>
        <field>OwnerId</field>
        <lookupValue>Recruiter_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign New Position To Recruiter Queue</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Opened_to_Today</fullName>
        <field>Open_Date__c</field>
        <formula>NOW()</formula>
        <name>Date Opened to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_to_Recruiter_Queue_When_Approved</fullName>
        <field>OwnerId</field>
        <lookupValue>Recruiter_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Owner to Recruiter Queue When Approved</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Closed_Date_to_Today</fullName>
        <field>Close_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Closed Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Closed_Not_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Closed - Not Approved</literalValue>
        <name>Set Status to Closed - Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Status_to_Open_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Open - Approved</literalValue>
        <name>Set Status to Open Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_sub_status_to_Approved</fullName>
        <field>Substatus__c</field>
        <literalValue>Approved</literalValue>
        <name>Set sub status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Closed_When_Not_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Closed - Not Approved</literalValue>
        <name>Status to Closed When Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Open</fullName>
        <field>Status__c</field>
        <literalValue>Open - Approved</literalValue>
        <name>Status to Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubStatus_to_Approved</fullName>
        <field>Substatus__c</field>
        <literalValue>Approved</literalValue>
        <name>SubStatus to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_status_for_position_in_progress</fullName>
        <field>Substatus__c</field>
        <literalValue>Pending</literalValue>
        <name>Sub status for position in progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Substatus_for_Position_in_Progress</fullName>
        <field>Substatus__c</field>
        <literalValue>Pending</literalValue>
        <name>Substatus for Position in Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Position Rule</fullName>
        <actions>
            <name>Assign_New_Position_To_Recruiter_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Position__c.Status__c</field>
            <operation>equals</operation>
            <value>New Position</value>
        </criteriaItems>
        <description>rule when new position is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>New_Position_Approved_Follow_Up</fullName>
        <assignedTo>rebekahtotten.recruiter@yahoo.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Once the manager of the record owner approves a new position, a task is created for the recruiter.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Position Approved - Follow-Up</subject>
    </tasks>
</Workflow>
