<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Contact_Status_History</fullName>
        <field>Contact_Status_History__c</field>
        <formula>IF( Contact_Status__c == TRUE, &apos;True&apos;, &apos;False&apos;)</formula>
        <name>Populate Contact Status History</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Contact Status History</fullName>
        <actions>
            <name>Populate_Contact_Status_History</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISNEW(), ISCHANGED(Contact_Status__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
