<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Coop_History</fullName>
        <field>Coop_History__c</field>
        <formula>Surviving_Entity__c</formula>
        <name>Populate Coop History</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Coop History</fullName>
        <actions>
            <name>Populate_Coop_History</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Surviving_Entity__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
