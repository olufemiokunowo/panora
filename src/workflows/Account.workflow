<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>OutboundAccountData</fullName>
        <apiVersion>34.0</apiVersion>
        <description>test outbound notification to external system</description>
        <endpointUrl>http://vibrantcontrols.com/login.aspx</endpointUrl>
        <fields>AccountNumber</fields>
        <fields>AccountSource</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>Phone</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>femiokunowo@gmail.com</integrationUser>
        <name>OutboundAccountData</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
</Workflow>
