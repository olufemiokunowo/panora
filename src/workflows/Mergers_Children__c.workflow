<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Surviving_Entity</fullName>
        <field>Surviving_Entity__c</field>
        <formula>IF(!ISBLANK( Originating_Account__r.Coop_History__c ), Originating_Account__r.Coop_History__c + &apos;, &apos; + Coop_Involved__r.Name, Coop_Involved__r.Name)</formula>
        <name>Populate Surviving Entity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Originating_Account__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Populate Surviving Entity</fullName>
        <actions>
            <name>Populate_Surviving_Entity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
