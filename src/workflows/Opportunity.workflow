<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Sales_Notification</fullName>
        <description>Sales Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Recruiter</recipient>
            <type>roleSubordinatesInternal</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesNewCustomerEmail</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Opty_Stage</fullName>
        <field>StageName</field>
        <literalValue>Value Proposition</literalValue>
        <name>Update Opty Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Approved_By_Supv</fullName>
        <field>StageName</field>
        <literalValue>Approved By Supervisor</literalValue>
        <name>Update Status to Approved  By Supv</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Pending_Supv_Approval</fullName>
        <field>StageName</field>
        <literalValue>Pending Supervisor Approval</literalValue>
        <name>Update Status to Pending Supv Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_to_Rejected_By_Supv</fullName>
        <field>StageName</field>
        <literalValue>Rejected By Supervisor</literalValue>
        <name>Update Status to Rejected  By Supv</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>qualtrics__Qualtrics_Example_Outbound_Message</fullName>
        <apiVersion>34.0</apiVersion>
        <description>An example of how to setup an outbound message. 
The endpoint url is not valid and needs to be updated to a real out endpoint url.</description>
        <endpointUrl>http://survey.qualtrics.com/WRQualtricsServer/sfApi.php?r=outboundMessage&amp;u=UR_123456789&amp;s=SV_123456789&amp;t=TR_123456789</endpointUrl>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>femiokunowo@gmail.com</integrationUser>
        <name>Qualtrics Example Outbound Message</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Opportunity Amount Greater Than 5M</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This workflow rule will be used to trigger a process flow</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>qualtrics__Qualtrics Example Survey Rule</fullName>
        <actions>
            <name>qualtrics__Qualtrics_Example_Outbound_Message</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>An example of how to setup a rule to trigger a survey using an outbound message. 
In this example when an opportunity is closed we want to email the opportunity and see how their interaction with the sales representative went.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>amount_Awaiting_Approval</fullName>
        <assignedTo>femiokunowo@gmail.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>amount Awaiting Approval</subject>
    </tasks>
</Workflow>
