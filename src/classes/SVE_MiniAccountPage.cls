global with sharing class SVE_MiniAccountPage extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public Account record {get{return (Account)mainRecord;}}
    public with sharing class CanvasException extends Exception {}

    
    
    public Component117 Component117 {get; private set;}
    public Component128 Component128 {get; private set;}
    public SVE_MiniAccountPage(ApexPages.StandardController controller) {
        super(controller);


        SObjectField f;

        f = Account.fields.OwnerId;
        f = Account.fields.Name;
        f = Account.fields.AccountNumber;
        f = Account.fields.Site;
        f = Account.fields.Type;
        f = Account.fields.AnnualRevenue;
        f = Account.fields.Rating;
        f = Account.fields.Phone;
        f = Account.fields.Fax;
        f = Account.fields.Website;
        f = Account.fields.Industry;
        f = Account.fields.Ownership;
        f = Account.fields.BillingStreet;
        f = Account.fields.BillingCity;
        f = Account.fields.BillingState;
        f = Account.fields.BillingPostalCode;
        f = Account.fields.BillingCountry;
        f = Account.fields.ShippingStreet;
        f = Account.fields.ShippingCity;
        f = Account.fields.ShippingState;
        f = Account.fields.ShippingPostalCode;
        f = Account.fields.ShippingCountry;
        f = Contact.fields.LastName;
        f = Contact.fields.Title;
        f = Contact.fields.Email;
        f = Contact.fields.Phone;
        f = Contact.fields.FirstName;
        f = Opportunity.fields.Name;
        f = Opportunity.fields.StageName;
        f = Opportunity.fields.Amount;
        f = Opportunity.fields.CloseDate;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = Account.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('Account');
            mainQuery.addField('OwnerId');
            mainQuery.addField('Name');
            mainQuery.addField('AccountNumber');
            mainQuery.addField('Site');
            mainQuery.addField('Type');
            mainQuery.addField('AnnualRevenue');
            mainQuery.addField('Rating');
            mainQuery.addField('Phone');
            mainQuery.addField('Fax');
            mainQuery.addField('Website');
            mainQuery.addField('Industry');
            mainQuery.addField('Ownership');
            mainQuery.addField('BillingStreet');
            mainQuery.addField('BillingCity');
            mainQuery.addField('BillingState');
            mainQuery.addField('BillingPostalCode');
            mainQuery.addField('BillingCountry');
            mainQuery.addField('ShippingStreet');
            mainQuery.addField('ShippingCity');
            mainQuery.addField('ShippingState');
            mainQuery.addField('ShippingPostalCode');
            mainQuery.addField('ShippingCountry');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutSalesforce; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            Component117 = new Component117(new List<Contact>(), new List<Component117Item>(), new List<Contact>(), null);
            listItemHolders.put('Component117', Component117);
            query = new SkyEditor2.Query('Contact');
            query.addField('LastName');
            query.addField('Title');
            query.addField('Email');
            query.addField('Phone');
            query.addField('FirstName');
            query.addWhere('AccountId', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
            relationFields.put('Component117', 'AccountId');
            Component117.queryRelatedEvent = False;
            query.limitRecords(500);
            queryMap.put('Component117', query);
            
            Component128 = new Component128(new List<Opportunity>(), new List<Component128Item>(), new List<Opportunity>(), null);
            listItemHolders.put('Component128', Component128);
            query = new SkyEditor2.Query('Opportunity');
            query.addField('Name');
            query.addField('StageName');
            query.addField('Amount');
            query.addField('CloseDate');
            query.addWhere('AccountId', mainRecord.Id, SkyEditor2.WhereOperator.Eq);
            relationFields.put('Component128', 'AccountId');
            Component128.queryRelatedEvent = False;
            query.limitRecords(500);
            queryMap.put('Component128', query);
            
            registRelatedList('Contacts', 'Component117');
            registRelatedList('Opportunities', 'Component128');
            
            p_showHeader = true;
            p_sidebar = true;
            init();
            
            Component117.extender = this.extender;
            Component128.extender = this.extender;
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    

    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    private static testMethod void testPageMethods() {        SVE_MiniAccountPage extension = new SVE_MiniAccountPage(new ApexPages.StandardController(new Account()));
        SkyEditor2.Messages.clear();
        extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        SkyEditor2.Messages.clear();
        extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        SkyEditor2.Messages.clear();
        extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

        Integer defaultSize;
    }
    global with sharing class Component117Item extends SkyEditor2.ListItem {
        public Contact record{get; private set;}
        Component117Item(Component117 holder, Contact record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component117 extends SkyEditor2.ListItemHolder {
        public List<Component117Item> items{get; private set;}
        Component117(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component117Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component117Item(this, (Contact)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
    }
    private static testMethod void testComponent117() {
        Component117 Component117 = new Component117(new List<Contact>(), new List<Component117Item>(), new List<Contact>(), null);
        Component117.create(new Contact());
        Component117.doDeleteSelectedItems();
        System.assert(true);
    }
    
    global with sharing class Component128Item extends SkyEditor2.ListItem {
        public Opportunity record{get; private set;}
        Component128Item(Component128 holder, Opportunity record, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(holder);
            if (record.Id == null ){
                if (recordTypeSelector != null) {
                    recordTypeSelector.applyDefault(record);
                }
                
            }
            this.record = record;
        }
        global override SObject getRecord() {return record;}
        public void doDeleteItem(){deleteItem();}
    }
    global with sharing  class Component128 extends SkyEditor2.ListItemHolder {
        public List<Component128Item> items{get; private set;}
        Component128(List<SObject> records, List<SkyEditor2.ListItem> items, List<SObject> deleteRecords, SkyEditor2.RecordTypeSelector recordTypeSelector) {
            super(records, items, deleteRecords, recordTypeSelector);
            this.items = (List<Component128Item>)items;
        }
        global override SkyEditor2.ListItem create(SObject data) {
            return new Component128Item(this, (Opportunity)data, recordTypeSelector);
        }
        public void doDeleteSelectedItems(){deleteSelectedItems();}
    }
    private static testMethod void testComponent128() {
        Component128 Component128 = new Component128(new List<Opportunity>(), new List<Component128Item>(), new List<Opportunity>(), null);
        Component128.create(new Opportunity());
        Component128.doDeleteSelectedItems();
        System.assert(true);
    }
    
    public Contact Component117_table_Conversion { get { return new Contact();}}
    
    public String Component117_table_selectval { get; set; }
    
    with sharing class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}