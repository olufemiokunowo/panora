public class RestToXMLDemo {

    //public String endPointUrl = 'http://www.fpdsng.com/FPDS/BusinessServices/DataCollection/contracts';
    String fiscalYear	= '2017';
    String stateCode	= 'TX';
    String maxRecords	= '5';
    
    List<FPDS_Contract__c> contractList = new List<FPDS_Contract__c>();
    
    //String endPointUrl = 'https://www.usaspending.gov/fpds/fpds.php?detail=c&fiscal_year=2017&max_records=1000';
    
    String endPointUrl = 'https://www.usaspending.gov/fpds/fpds.php?detail=c'
        +  '&fiscal_year=' + EncodingUtil.urlEncode(fiscalYear,'UTF-8') 
        + '&mod_agency=' + '97DH' 
        +  '&max_records=' + EncodingUtil.urlEncode(maxRecords,'UTF-8')
        + '&sortby=d';
        

    // call the REST service with info
    public void callWebService() {

        HttpRequest req = new HttpRequest();
        Http h = new Http();

        // set the request method
        req.setMethod('GET');
        req.setHeader('Accept','application/json');
        req.setHeader('Content-type','application/xml; charset=UTF-8');
        req.setHeader('Accept-Language', 'en-US');
        req.setTimeout(120000);

        /*
        String url = 'http://local.yahooapis.com/MapsService/V1/geocode?appid=' + appId
            + '&amp;street=' + EncodingUtil.urlEncode(street,'UTF-8')
            + '&amp;city=' + EncodingUtil.urlEncode(city,'UTF-8')
            + '&amp;state=' + EncodingUtil.urlEncode(state,'UTF-8');
		*/
        
        string url = endPointUrl; // + '/getListContractResponseType';
        
        // add the endpoint to the request
        req.setEndpoint(url);

        // create the response object
        HTTPResponse resp = h.send(req);
        if(resp.getStatusCode() != 200)
            return;
        
        Dom.Document doc = resp.getBodyDocument();
        if(doc == null)
            return;
        
        // retrieve the root element for this document
        Dom.XMLNode contract = doc.getRootElement();    
        Dom.XmlNode [] childElements = contract.getChildElements() ; //Get all Record Elements
		
        for(Dom.XMLNode childElem : childElements) //Loop Through Records
        {
            if (childElem.getNodeType() == dom.XmlNodeType.ELEMENT && childElem.getName() == 'result')
            {
                 //system.debug('childElem ' + childElem);
                parseSecondLevelChildNode(childElem);  
            }
                   
        }
        
        if(contractList.size() > 0)
        {
            system.debug('contractList ' + contractList);
            system.debug('contractList size: ' + contractList.size());
            //insert contractList;
        }
           
    }
    
    
    private void parseSecondLevelChildNode(DOM.XMLNode childNode) {
        
        List<Dom.XmlNode> subChildNode = childNode.getChildren();
        //system.debug('subChildNode ' + subChildNode);
        Set<String> contraKey = new Set<String>();
        
        if (subChildNode != null && !subChildNode.isEmpty())
        {
             
            for (Dom.XmlNode n : subChildNode) 
            {
                FPDS_Contract__c newContract = new FPDS_Contract__c();
                String modNumber;
                String piid;
                String obligatedAmt;
                //system.debug('n : ' + n);
                if (n.getNodeType() == dom.XmlNodeType.ELEMENT && n.getName() == 'doc')
                {
                   
                    system.debug('we found doc element');
                    //system.debug('n ' + n);
                    List<Dom.XmlNode> n2 = n.getChildren();
                    system.debug('n2 ' + n2);
                   
                   
                    for (Dom.XmlNode n3 : n2)
                    {
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'obligatedAmount' )
                        {
                        	obligatedAmt = n3.gettext().trim();
                            newContract.DollarsObligated__c = Decimal.valueOf(obligatedAmt);
                            //String amt = n3.getChildElement('obligatedAmount', null).getText();
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'modNumber' )
                        {
                        	modNumber = n3.gettext().trim();
                            newContract.Mod_Number__c = n3.gettext().trim();
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'maj_agency_cat')
                        {
                            newContract.Major_Agency_Category__c = n3.gettext().trim();
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'fiscal_year')
                        {
                            newContract.FiscalYear__c = Integer.valueOf(n3.gettext().trim());
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'vendorName')
                        {
                            newContract.Vendor_Name__c =n3.gettext().trim();
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'effectiveDate')
                        {
                            newContract.Effective_Date__c =parseDate(n3.gettext().trim()); // IDVPIID
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'PIID')
                        {
                            
                            piid = n3.gettext().trim();
                            newContract.PIID__c = n3.gettext().trim();
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'IDVPIID')
                        {
                            newContract.IDVPIID__C =n3.gettext().trim(); 
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                    }
                   
                }
                
                if(!contraKey.contains(piid+modNumber+obligatedAmt))
                {
                    system.debug('Adding PIID, Mod Number, Obligated Amount: ' + piid + ' ' + modNumber + ' ' + obligatedAmt);
                    contractList.add(newContract);
                    contraKey.add(piid+modNumber+obligatedAmt);
                }
                
            }
        }
        
    }
    
    private Datetime parseDate(string dateToParse)
    {
        List<String> dateParts = dateToParse.split('-');
        Datetime newDate = Datetime.newInstance(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[2].left(2)));
        return newDate;
    }

    /* utility method to convert the xml element to the inner class
    private GeoResult toGeoResult(TG_XmlDom.Element element) {

        GeoResult geo = new GeoResult();
        geo.latitude = element.getValue('Latitude');
        geo.longitude = element.getValue('Longitude');
        geo.address = element.getValue('Address');
        geo.city = element.getValue('City');
        geo.state = element.getValue('State');
        geo.zip = element.getValue('Zip');
        return geo;
    }

	*/
    // inner class
    private class GeoResult {

        public String latitude;
        public String longitude;
        public String address;
        public String city;
        public String state;
        public String zip;
        public String toDisplayString() {
            return address + ', '
            + city + ', '
            + state + ', '
            + zip + ' ['
            + latitude + ', '
            + longitude + ']';
        }

    }

    
}