public class SampleWebService {
  public static Http httpObject;
  public static HttpRequest request;
  public static HttpResponse response;
  
  public static void makeRequest(String financeNumber, String permitNumber) {
    httpObject = new Http();
    request = new HttpRequest();
    response = new HttpResponse();

    request.setMethod('POST');
    request.setEndpoint('callout:WebChefOne');
    request.setHeader('Accept','application/json');
    request.setHeader('Content-type','application/xml; charset=UTF-8');
    request.setHeader('Accept-Language', 'en-US');
    request.setBody(makeXmlBody(financeNumber, permitNumber));
    request.setTimeout(2000);

    try {
      response = httpObject.send(request);
      if (response.getStatusCode() < 300) {
        handleResponse(financeNumber, permitNumber);
      } else {
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Occured in the transaction with the web service. Please try again. If it continues please contact support. Error Code ' + response.getStatusCode()));
      }
    } catch (Exception ex) {
      //ApexPages.addMessages(error);
      //CFRExceptionUtility.createRecordsFromExceptions(error);
    }
  }

  /*
   * Check if the status code was 200 or 204 or something else.
   * Posting error messages accordingly.
   */
  private static void handleResponse(String financeNumber, String permitNumber) {
    JSONParser parser = JSON.createParser(response.getBody());

    while (parser.nextToken() != null) {
      if (parser.getCurrentToken() == JSONToken.FIELD_NAME &&
        parser.getText() == 'statusCode') {
        parser.nextToken();
        if (parser.getIntegerValue() == 204) {
          //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid combination for finance number ' + financeNumber + ' and permit number ' + permitNumber + '.'));
          return;
        } else if (parser.getIntegerValue() == 200) {
          String name = '';
          while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME &&
              parser.getText() == 'name') {
              parser.nextToken();
              name = parser.getText();
            }
          }
          /*ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Valid combination for finance number ' + financeNumber +
                                            ' and permit number ' + permitNumber + ' for the company ' +
                                            name + '.'));
		  */
          return;
        }
      }
    }
  }

  private static String makeXmlBody(String financeNumber, String permitNumber) {
    String xmlBody = '<?xml version="1.0" encoding="UTF-8"?>';
    xmlBody += '<adm:companyByPermitRequest  xmlns:adm="http://ws.admin.webchef.com/jaxb" xmlns:adm_base="http://ws.admin.webchef.com/jaxb/base" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://ws.admin.webchef.com/jaxb admin_v1.xsd ">';
    xmlBody += '<appAuthInput>';
    xmlBody += '<appName></appName>';
    xmlBody += '<appPassword></appPassword>';
    xmlBody += '</appAuthInput>';
    xmlBody += '<requestInfo>';
    xmlBody += '<permitType>PI</permitType>';
    xmlBody += '<permitNumber>' + permitNumber + '</permitNumber>';
    xmlBody += '<financeNumber>' + financeNumber + '</financeNumber>';
    xmlBody += '</requestInfo>';
    xmlBody += '</adm:companyByPermitRequest>';

    return xmlBody;
  }
}