public class NewInvestigationRedirectController {

    static Id COH_INV_REC_TYPE_ID = Schema.SObjectType.Investigation__c.getRecordTypeInfosByName().get('Cohort Investigation').getRecordTypeId();
                
    private ApexPages.StandardController  controller = null;
    public NewInvestigationRedirectController()
    {
        this.controller = controller;
    }
    
    public NewInvestigationRedirectController(ApexPages.StandardController controller)
    {
        this.controller = controller;
    }
    public PageReference redirectPage()
    {
        PageReference newPage;
        system.debug('cohort record type ID is ' + COH_INV_REC_TYPE_ID);
        string invObjPrefix = Investigation__c.SObjectType.getDescribe().getKeyPrefix();
        system.debug('Investigation Prefix is ' + invObjPrefix);        
        String cohortId = null;
        String cfLookUpKeyId = null;
        String cfLookUpNameKey = null;
        String parentCohortName = null;
        // loop through list of parameters and see if cohort name is passed to page
        Map<String, String> params = ApexPages.currentPage().getParameters();
        for (String key : params.keySet()) {
            if (key.startsWith('CF') && key.endsWith('lkid')) 
            {
                String val = params.get(key);    
                cfLookUpKeyId = key;
                cohortId = val;
            }
            else if (key.startsWith('CF') && !key.endsWith('lkid')) 
            {
                String nameValue = params.get(key);    
                cfLookUpNameKey = key;
                parentCohortName = nameValue;
            }
        }
        
        system.debug('cohort id is ' + cohortId );
        system.debug('cohort lookup key Id is ' + cfLookUpKeyId );
        system.debug('parent cohort name' + parentCohortName );
        system.debug('cohort lookup key ' + cfLookUpNameKey );
        
        if(cohortId != null && cfLookUpKeyId != null)
        {
           
            String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
            system.debug('Host name is ' + sfdcBaseURL);
            newPage = new PageReference('/' + invObjPrefix + '/e?');         
            newPage.getParameters().put(cfLookUpNameKey, parentCohortName);            
            newPage.getParameters().put(cfLookUpKeyId, cohortId);
            newPage.getParameters().put('saveURL', cohortId);
            newPage.getParameters().put('retURL', cohortId);
            newPage.getParameters().put('RecordType', COH_INV_REC_TYPE_ID);
            newPage.getParameters().put('nooverride', '1');
          
            return newPage.setRedirect(true);
        }
        else
        { 
            newPage = new PageReference('/' + invObjPrefix + '/e?');
            newPage.getParameters().put('nooverride', '1');
            newPage.getParameters().put('RecordType', COH_INV_REC_TYPE_ID);
           return newPage.setRedirect(true);
        }
        
        
    }

}