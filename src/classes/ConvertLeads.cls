public class ConvertLeads {
    public void leadsToConvert()
    {
         for(Lead myLead : [SELECT Id, Name FROM Lead WHERE Status = 'Sales Won'])
         {
             Database.LeadConvert lc = new database.LeadConvert();
             lc.setLeadId(myLead.id);
             lc.setConvertedStatus('Closed - Converted');
             
             LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
             lc.setConvertedStatus(convertStatus.MasterLabel);
             Database.LeadConvertResult lcr = Database.convertLead(lc);
             System.assert(lcr.isSuccess());
         }
    }
   
    
    
}