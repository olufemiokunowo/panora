public class XMLParsingDemo {

    public  final Integer DEPTH_TILL_LEAF = -1;
    public  final Integer DEPTH_BASE = 0;
    public  final Integer MAX_ELEMENTS_TILL_LEAF = -1; 
    
    public void testParse()
    {   
        String s = '<advertisers type="array">' +
            '<advertiser>   <id type="integer">7</id>  <name>ABC Telecom</name>   </advertiser> ' +
            '<advertiser>   <id type="integer">106</id>  <name>ABC_Ozone</name>  </advertiser> ' +
            '<advertiser>   <id type="integer">13</id>   <name>Acme Corp</name>  </advertiser> ' + 
            '</advertisers> ';
        
        List<Account> newaccounts = new List<Account> ();
        
        Dom.Document docx = new Dom.Document();
        docx.load(s);
        dom.XmlNode xroot = docx.getrootelement() ;
        system.debug('xRoot :' + xroot);
        
        
        dom.XmlNode [] xrec = xroot.getchildelements() ; //Get all Record Elements
        system.debug('xrec : ' + xrec);
        
        for(Dom.XMLNode child : xrec) //Loop Through Records
        {
            account a = new Account ();
            
            for (dom.XmlNode awr : child.getchildren() ) {
                system.debug('awr.getname() is ' + awr.getname());
                system.debug('awr.gettext() is ' + awr.gettext());
                
                if (awr.getname() == 'id') {
                    system.debug('Id' + awr.gettext());
                    a.accountnumber = awr.gettext();
                }  
                
                if (awr.getname() == 'name') {
                    system.debug('name' + awr.gettext());
                    a.name = awr.gettext();
                }  
                
                
            }
            newaccounts.add(a);
        }
        system.debug(newaccounts);//you could insert here or upsert based on ID with an external Id field
    }
    
    
    public void testParse2()
    {  
        String ss = '<crm><advertisers type="array">' +
            '<advertiser>   <id type="integer">7</id>  <name>ABC Telecom</name>   </advertiser> ' +
            '<advertiser>   <id type="integer">106</id>  <name>ABC_Ozone</name>  </advertiser> ' +
            '<advertiser>   <id type="integer">13</id>   <name>Acme Corp</name>  </advertiser> ' + 
            '</advertisers>' +
            '<vendors type="array">' +
            '<vendor>   <vendorid pryKey="1" type="integer">1000</vendorid>  <vendorname>Geico</vendorname>   </vendor> ' +
            '<vendor>   <vendorid pryKey="2" type="integer">1001</vendorid>  <vendorname>State Farms</vendorname>  </vendor> ' +
            '<vendor>   <vendorid pryKey="3" type="integer">1002</vendorid>   <vendorname>Marine</vendorname>  </vendor> ' + 
            '</vendors>' +
            '</crm>';
        
        List<Account> newaccounts = new List<Account> ();
        
        Dom.Document docx = new Dom.Document();
        docx.load(ss);
             
        dom.XmlNode xroot = docx.getrootelement() ;
        //system.debug('xRoot :' + xroot);
        
        dom.XmlNode [] xrec = xroot.getchildelements() ; //Get all Record Elements
        //system.debug('xrec : ' + xrec);
        
                
        for(Dom.XMLNode child : xrec) //Loop Through Records
        {
            account a = new Account ();
            
            List<Dom.XmlNode> xx = child.getChildren();
            //system.debug('xx ' + xx);
            
             if (xx != null && !xx.isEmpty())
             {
                 for (Dom.XmlNode n : xx) 
                 {
                     //system.debug('n : ' + n);
                     if (n.getNodeType() == dom.XmlNodeType.ELEMENT && n.getName() == 'vendor')
                     {
                         system.debug('we found vendor element');
                         List<Dom.XmlNode> n2 = n.getChildren();
                         
                         for (Dom.XmlNode n3 : n2)
                         {
                             if(String.isNotBlank(n3.getname()))
                             {
                                 system.debug('n3.getname() is ' + n3.getname());
                                 //n3.getAttributeValue();
                             }
                             if(String.isNotBlank(n3.gettext()))
                             {
                                 system.debug('n3.gettext() is ' + n3.gettext());
                             }
                             
                         }
                 
                        
                     }
                 }
             }
		
        }
        

    }
    
   
    public void testParse3()
    {  
        String ss = '<crm><advertisers type="array">' +
            '<advertiser>   <id type="integer">7</id>  <name>ABC Telecom</name>   </advertiser> ' +
            '<advertiser>   <id type="integer">106</id>  <name>ABC_Ozone</name>  </advertiser> ' +
            '<advertiser>   <id type="integer">13</id>   <name>Acme Corp</name>  </advertiser> ' + 
            '</advertisers>' +
            '<vendors type="array">' +
            '<vendor>   <vendorid type="integer">1000</vendorid>  <vendorname>Geico</vendorname>   </vendor> ' +
            '<vendor>   <vendorid type="integer">1001</vendorid>  <vendorname>State Farms</vendorname>  </vendor> ' +
            '<vendor>   <vendorid type="integer">1002</vendorid>   <vendorname>Marine</vendorname>  </vendor> ' + 
            '</vendors>' +
            '<result>' +
            	'<doc>' +
            		'<amount>50000</amount>' +
            		'<agency>DHA</agency>' +
            	'</doc>' + 
            	'<doc>' +
            		'<amount>20000</amount>' +
            		'<agency>DOD</agency>' +
            	'</doc>' + 
            '</result>' + 
            '</crm>';
        
        List<Account> newaccounts = new List<Account> ();
        
        Dom.Document docx = new Dom.Document();
        docx.load(ss);
               
        dom.XmlNode xroot = docx.getrootelement() ;
        //system.debug('xRoot :' + xroot);
        
        //dom.XmlNode xroot2 = xroot.g
        
        dom.XmlNode [] xrec = xroot.getchildelements() ; //Get all Record Elements
        //system.debug('xrec : ' + xrec);
        
        
        
        for(Dom.XMLNode child : xrec) //Loop Through Records
        {
            account a = new Account ();
            
            List<Dom.XmlNode> xx = child.getChildren();
            //system.debug('xx ' + xx);
            
            if (xx != null && !xx.isEmpty())
            {
                for (Dom.XmlNode n : xx) 
                {
                    //system.debug('n : ' + n);
                    if (n.getNodeType() == dom.XmlNodeType.ELEMENT && n.getName() == 'doc')
                    {
                        system.debug('we found doc element');
                        List<Dom.XmlNode> n2 = n.getChildren();
                        for (Dom.XmlNode n3 : n2)
                        {
                            if(String.isNotBlank(n3.getname()))
                            {
                                system.debug('n3.getname() is ' + n3.getname());
                            }
                            if(String.isNotBlank(n3.gettext()))
                            {
                                system.debug('n3.gettext() is ' + n3.gettext());
                            }
                            
                        }
                        
                        
                    }
                }
            }
            
        }
        
        
    }
    
    private void parseParentNode(DOM.XMLNode node) {
        for (Dom.XMLNode child : node.getChildElements()) {
            if (child.getName() == 'monthlyReport') {
                parseChildrenNode(child);
                //reports.add(r);
            }
        }
    }
    
    private void parseChildrenNode(DOM.XMLNode node) {
        for (Dom.XMLNode child : node.getChildElements()) {
            if (child.getName() == 'month') {
                system.debug('value: ' + child.getText().trim());
            } 
        }
    }
    

   
}