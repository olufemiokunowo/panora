public class RestToXMLDemo2Nested {

    String lastModDate	= 'LAST_MOD_DATE:[2013/10/01,2014/09/30]';
    String xmlNameSpace = EndPoint_URL__c.getValues('FPDS Namespace').URL__c;
    String endPoint = EndPoint_URL__c.getValues('FPDS WebService').URL__c;
    String atom = 'http://www.w3.org/2005/Atom';
    List<DOM.XMLNode> entryNodes = new List<DOM.XMLNode>();
    List<DOM.XMLNode> contentNodes = new List<DOM.XMLNode>();
    List<DOM.XMLNode> awardNodes = new List<DOM.XMLNode>();
    
 
    //List<FPDS_Contract__c> contractList = new List<FPDS_Contract__c>();
    List<DISSContractWrapper> contractList = new List<DISSContractWrapper>();
    String endPointUrl = endPoint + lastModDate;
    
    /*
    String endPointUrl = 'https://www.fpds.gov/ezsearch/FEEDS/ATOM?FEEDNAME=PUBLIC&q='
        +  '&fiscal_year=' + EncodingUtil.urlEncode(fiscalYear,'UTF-8');
    */   
        

    // call the REST service with info
    public void callWebService() {

        HttpRequest req = new HttpRequest();
        Http h = new Http();

        // set the request method
        req.setMethod('GET');
        req.setHeader('Accept','application/json');
        req.setHeader('Content-type','application/xml; charset=UTF-8');
        req.setHeader('Accept-Language', 'en-US');
        req.setTimeout(120000);

        string url = endPointUrl; // + '/getListContractResponseType';
        
        // add the endpoint to the request
        req.setEndpoint(url);

        // create the response object
        HTTPResponse resp = h.send(req);
        if(resp.getStatusCode() != 200)
            return;
        
        Dom.Document doc = resp.getBodyDocument();
        if(doc == null)
            return;
        
        /** this section is work in progress
         
         // retrieve the root element for this document
        Dom.XMLNode env = doc.getRootElement();    
        //Dom.XmlNode [] childElements = env.getChildElements() ; //Get all Record Elements
		
        Dom.XmlNode  entryElement = env.getChildElement('entry', atom);
        
        
        Dom.XmlNode  contentElement = entryElement.getChildElement('content', atom);
        
        Dom.XmlNode  awardElement = contentElement.getChildElement('award', xmlNameSpace);
        Dom.XmlNode  awardIdElement = contentElement.getChildElement('awardId', xmlNameSpace);
        system.debug('awardIdElement ' + awardIdElement);

         */
       
        //parse the child elements from the feed root
        for(Dom.XMLNode childElem : doc.getRootElement().getChildElements()) //Loop Through Records
        {
            system.debug('childElem is ' + childElem.getName());
            if (childElem.getNodeType() == dom.XmlNodeType.ELEMENT && childElem.getName().toLowerCase() == 'entry')
            {
                system.debug('Found entry element - add to content');
                entryNodes.add(childElem);
            }      
        }
        
        // process entry nodes
        if(entryNodes.size() > 0)
        {
            system.debug('Found entry nodes - Parse the entryNodes to obtain content nodes');
            parseChildrenNode(entryNodes, 'entry');
        }
        
         // process content nodes
        if(contentNodes.size() > 0)
        {
            system.debug('Found content nodes - Parse the contentNodes to obtain award nodes');
            parseChildrenNode(contentNodes, 'content');
        }
        
        if(awardNodes.size() > 0)
        {
            system.debug('Found Award Nodes - Parse the award nodes to retrieve values needed for contract');
            parseSubChildrenNode(awardNodes, 'award');
        }
		
        
        /*
        if(contractList.size() > 0)
        {
            system.debug('contractList ' + contractList);
            system.debug('contractList size: ' + contractList.size());
            //insert contractList;
        }
		*/
           
    }
    
    
    private void parseChildrenNode(List<DOM.XMLNode> nodes, String parentElemName)
    {
        for(DOM.XMLNode node : nodes)
        {
            if(parentElemName == 'entry')
            {
                system.debug('Processing entry nodes to retrieve content nodes');
                contentNodes.add(node);
            }
            if(parentElemName == 'content')
            {
                system.debug('Processing content nodes to retrieve award nodes');
                awardNodes.add(node);
            }
        }
    }
    
    private void parseSubChildrenNode(List<DOM.XMLNode> nodes, String parentElemName)
    {
        for(DOM.XMLNode node : nodes)
        {
            if(parentElemName == 'award' && node.getNodeType() == dom.XmlNodeType.ELEMENT && node.getName().toLowerCase() == 'award')
            {
                parseSecondLevelChildrenNode(node);
            }
        }
    }
    
    private void parseSecondLevelChildrenNode(DOM.XMLNode childNode) {
        
        List<Dom.XmlNode> subChildNode = childNode.getChildren();	// award nodes
        Set<String> contraKey = new Set<String>();
        
        if (subChildNode != null && !subChildNode.isEmpty())
        {
            // pass each element of the award nodes
            for (Dom.XmlNode n : subChildNode) 
            {
                DISSContractWrapper wrapper = new DISSContractWrapper();
                String modNumber;
                String piid;
                String obligatedAmt;
                system.debug('n : ' + n);
                
                if (n.getNodeType() == dom.XmlNodeType.ELEMENT && n.getName().toLowerCase() == 'awardid')
                {
                    List<Dom.XmlNode> nodeChildren = n.getChildren();	// get the children of awardid which is awardcontractid
                    if(nodeChildren.size() > 0)
                    {
                        //List<Dom.XmlNode> levelTwoNodeChildren = nodeChildren.getChildren();
                    }
                    for (Dom.XmlNode nn : subChildNode) 
                     {
                         if(String.isNotBlank(nn.getname()) && String.isNotBlank(nn.gettext()) && nn.getName() == 'obligatedAmount')
                         {
                             obligatedAmt = nn.gettext().trim();
                             wrapper.ObligatedAmount = Decimal.valueOf(obligatedAmt);
                             system.debug('nn.getName() ' + nn.getName());
                             system.debug('nn.getText() ' + nn.getText());
                         }                       
                     }
                }
                
                if (n.getNodeType() == dom.XmlNodeType.ELEMENT && n.getName().toLowerCase() == 'dollarvalues')
                {
                    List<Dom.XmlNode> nodeChildren = n.getChildren();	// get the children of dollarValues element
                    for (Dom.XmlNode nn : subChildNode) 
                     {
                         if(String.isNotBlank(nn.getname()) && String.isNotBlank(nn.gettext()) && nn.getName() == 'obligatedAmount')
                         {
                             obligatedAmt = nn.gettext().trim();
                             wrapper.ObligatedAmount = Decimal.valueOf(obligatedAmt);
                             system.debug('nn.getName() ' + nn.getName());
                             system.debug('nn.getText() ' + nn.getText());
                         }                       
                     }
                }
                if (n.getNodeType() == dom.XmlNodeType.ELEMENT && n.getName().toLowerCase() == 'purchaserinformation')
                {
                    List<Dom.XmlNode> nodeChildren = n.getChildren();	// get the children of purchaserinformation element
                    for (Dom.XmlNode nn : subChildNode) 
                    {
                        if(nn.getName().toLowerCase() == 'contractingofficeagencyid ')
                        {
                            wrapper.ContractingAgencyID = nn.gettext().trim();
                            wrapper.ContractingAgency	= nn.getAttributeValue('name', '');
                            system.debug('nn.getName() ' + nn.getName());
                            system.debug('nn.getText() ' + nn.getText());
                        }
                        if(nn.getName().toLowerCase() == 'fundingrequestingagencyid')
                        {
                            wrapper.FundingOfficeID		= nn.gettext().trim();
                            wrapper.FundingOfficeName	= nn.getAttributeValue('name', '');
                            system.debug('nn.getName() ' + nn.getName());
                            system.debug('nn.getText() ' + nn.getText());
                        }
                        
                    }
                }
                
                //remove all the one below
                
                if (n.getNodeType() == dom.XmlNodeType.ELEMENT && n.getName() == 'awardContractID')
                {
                   
                    system.debug('we found awardContractID element');
                    //system.debug('n ' + n);
                    List<Dom.XmlNode> n2 = n.getChildren();
                    system.debug('n2 ' + n2);
                   
                   
                    for (Dom.XmlNode n3 : n2)
                    {
                      
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'modNumber' )
                        {
                        	modNumber = n3.gettext().trim();
                            //newContract.Mod_Number__c = n3.gettext().trim();
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'maj_agency_cat')
                        {
                            //newContract.Major_Agency_Category__c = n3.gettext().trim();
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'fiscal_year')
                        {
                            //newContract.FiscalYear__c = Integer.valueOf(n3.gettext().trim());
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'vendorName')
                        {
                            //newContract.Vendor_Name__c =n3.gettext().trim();
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'effectiveDate')
                        {
                            //newContract.Effective_Date__c =parseDate(n3.gettext().trim()); // Invalid date/time: 2014-11-21T00:00:00.000
                        }
                        if(String.isNotBlank(n3.getname()) && String.isNotBlank(n3.gettext()) && n3.getName() == 'PIID')
                        {
                            
                            piid = n3.gettext().trim();
                            //newContract.PIID__c = n3.gettext().trim();
                            system.debug('n3.getName() ' + n3.getName());
                            system.debug('n3.getText() ' + n3.getText());
                        }
                    }
                   
                }
                
                if(!contraKey.contains(piid+modNumber+obligatedAmt))
                {
                    system.debug('Adding PIID, Mod Number, Obligated Amount: ' + piid + ' ' + modNumber + ' ' + obligatedAmt);
                    //contractList.add(newContract);
                    contraKey.add(piid+modNumber+obligatedAmt);
                }
                
            }
        }
        
    }
    
    private Datetime parseDate(string dateToParse)
    {
        List<String> dateParts = dateToParse.split('-');
        Datetime newDate = Datetime.newInstance(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[2].left(2)));
        return newDate;
    }
    
    public class DISSContractWrapper {
        public String PIID;
        public String Category;
        public String FundingOfficeID;
        public String ContractingAgencyID;
        public String PSC;
        public String ReferencedIdvPIID;
        public String FundingOfficeName;
        public String ContractingAgency;
        public String PSCDescription;
        public String DunsNumber;
        public Double ObligatedAmount;
        public String FiscalYear;
        public String ModNumber;
        public String VendorName;
        public String ContractNumber;
        
    }

}