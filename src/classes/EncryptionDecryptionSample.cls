public class EncryptionDecryptionSample {
	
	
	String inputData = 'Test Data';
	public Blob dataToEncrypt = Blob.valueOf(inputData);
	
	// Use generateAesKey to generate the private key
	public Blob key = Crypto.generateAesKey(128);	
	public String b64Data {get;set;}
	public Blob encryptedData {get;set;}
	public Blob decryptedData {get;set;}
	public String b64Decrypted {get;set;}
	
	public void EncryptionDecryptionSample()
	{			
		
		System.debug('Data to encrypt in blob: ' + dataToEncrypt);
		// Generate an encrypted form of the data using base64 encoding
		//b64Data = EncodingUtil.base64Encode(dataToEncrypt);
				
		// Encrypt and decrypt the data
		encryptedData = Crypto.encryptWithManagedIV('AES128', key, dataToEncrypt);
		decryptedData = Crypto.decryptWithManagedIV('AES128', key, encryptedData);
		//b64Decrypted = EncodingUtil.base64Encode(decryptedData);		
		System.debug('Encrypted data is: ' + decryptedData);
		System.debug('Dencrypted data is: ' + encryptedData);
	}

}