public class CustomFieldAPIGet {

    public static list<map<string,object>> getFieldMetaData(string fieldName)
    {
        list<map<string,object>> results = new list<map<string,object>>();
        
        fieldName = fieldName.replace('__c','');
        string instanceURL = System.URL.getSalesforceBaseUrl().getHost().remove('-api' );
        
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
        req.setHeader('Content-Type', 'application/json');
        
        String toolingendpoint = 'https://'+instanceURL+'/services/data/v38.0/tooling/';
        
        //query for custom fields
        toolingendpoint += 'query/?q=Select+id,DeveloperName,FullName+from+CustomField+where+DeveloperName+=+\''+fieldName+'\'';
        req.setEndpoint(toolingendpoint);
        req.setMethod('GET');
        
        Http h = new Http();
        HttpResponse res = h.send(req);
        
        //convert the original data structure into a map of objects. The data we want is in the records property of this object
        map<string,object> reqData = (map<string,object>) json.deserializeUntyped(res.getBody());
        
        //now create a list of objects from the records property. This serialize/deserialize trick is the only way I know to convert a generic object
        //into something else when the source data is 'salesforce Map encoded' (no quotes around field names or values, parenthesis to denote open and close, etc)
        list<object> fieldData = (list<object>) JSON.deserializeUntyped(JSON.serialize(reqData.get('records')));    
        
        //iterate over each object in the list and create a map of string to object out of it and add it to the list
        for(object thisObj : fieldData)
        {
            system.debug('thisObj' + thisObj);
            map<string, object> thisFieldData =  (map<string, object>) json.deserializeUntyped(JSON.serialize(thisObj));
            system.debug('thisFieldData' + thisFieldData);
            results.add(thisFieldData);
        }
   
        return results;    
    }
}