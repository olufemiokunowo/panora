// ********************************************************************************************************************

// * NewInvestigationRedirectControllerTest   

// * 09/13/2016     Femi Okunowo   New  

// * Description	Test class class for NewInvestigationRedirectController class

// ********************************************************************************************************************



@isTest(SeeAllData=true)

public class NewInvestigationRedirectControllerTest {
    
    public Static URL_Parameters__c urlParam	= URL_Parameters__c.getOrgDefaults();
    public Static String cohortNameFieldId			= urlParam.Investigation_Cohort_Name__c;
    
        
    public static testMethod void testNewCIPageRedirect()   
    {     
        Id OPEN_COHORT = Schema.SObjectType.Cohort__c.getRecordTypeInfosByName().get('Open Cohort').getRecordTypeId(); 
        Id COH_INV_REC_TYPE_ID = Schema.SObjectType.Investigation__c.getRecordTypeInfosByName().get('Cohort Investigation').getRecordTypeId(); 
        String IN_PROGRESS = 'In Progress';         
        
        
        
        //build cohort        
        Cohort__c coh = new Cohort__c(Cohort_Name__c = 'Test Cohort',  CohortStatus__c = 'New' );
        insert coh;        
        // build investigation
        
        Investigation__c newInvestigation = new Investigation__c(Investigation_Name__c = 'TestSI',                                                                  
                                                                Status__c = IN_PROGRESS,
                                                                 CohortName__c = coh.Id);
        
        insert newInvestigation;
        
        Investigation__c getInv = [SELECT Id, Name FROM INVESTIGATION__C WHERE Id=: newInvestigation.Id LIMIT 1];    
        PageReference pageRef = Page.InvestigationRedirectPage;
        
        Test.setCurrentPage(pageRef);
        NewInvestigationRedirectController controller = new NewInvestigationRedirectController();
        ApexPages.StandardController  contrl = new ApexPages.StandardController(getInv);
        
        NewInvestigationRedirectController controller2 = new NewInvestigationRedirectController(contrl);        
        string invObjPrefix = Investigation__c.SObjectType.getDescribe().getKeyPrefix(); 
        
        String cfLookUp = 'CF' + cohortNameFieldId;
        String cfLookUpId = cfLookUp + '_lkid';
        
        ApexPages.currentPage().getParameters().put('nooverride', '1');        
        ApexPages.currentPage().getParameters().put('RecordType', COH_INV_REC_TYPE_ID);
        ApexPages.currentPage().getParameters().put(cfLookUpId, coh.Id);
        ApexPages.currentPage().getParameters().put(cfLookUp, coh.Name);
        
        
        controller2.redirectPage();        
    }

    
}