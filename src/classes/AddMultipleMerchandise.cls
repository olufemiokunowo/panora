public with sharing class AddMultipleMerchandise {
public PageReference SaveMultipleMerchandise() {
    PageReference pr = new PageReference('/apex/AddMultipleMerchandise');
    system.debug('controller save method is calling-->');
     AddMultipleMerchandiseHelper.save(waAccList);
    //return null;
    pr.setRedirect(True);
    return pr;
    }


 public List<WrapperpaMerchandiseList> waAccList {get;set;}
 public Integer rowToRemove {get;set;}

 public AddMultipleMerchandise(){
  waAccList = new List<WrapperpaMerchandiseList>();
  addNewRowToAccList();
 }
 public void removeRowFromAccList(){
  waAccList = AddMultipleMerchandiseHelper.removeRowToAccountList(rowToRemove, waAccList);
   
 }

 public void addNewRowToAccList(){
    waAccList = AddMultipleMerchandiseHelper.addNewRowToAccList(waAccList);
    }
    
    

 public class WrapperpaMerchandiseList{
        public Integer index {get;set;}
        public Merchandise__c record {get;set;}
   } 
}