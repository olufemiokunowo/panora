public class addUserAsContact {
        
        private static final String accountName = 'NRUCFC Internal';
        
        @future
        public static void addUserToContact(String firstName, String lastName, string email)
        {
            List<Contact> isContactAddedAlready = new List<Contact>(
                [Select Id from Contact where LastName =: lastName AND Email =: email]);
            
            system.debug('Size is ' + isContactAddedAlready.size());
            // check if contacts are already added
            if (isContactAddedAlready.size() == 0)
            {
                Contact userContact = new Contact(FirstName = firstName, LastName = lastName, 
                                                  Email = email, Internal_User__c = true);
                insert userContact;
            }            
            
        }
    
    @future
        public static void addUserToContact(List<String> Ids)
        {
            Id acctId = null;
            
            
            //List<Account> acct = new List<Account>([Select Id from Account Where Name = 'CFC' AND Organization_Legal_Name__c = 'National Rural Utilities Cooperative Finance Corporation' Limit 1]);
            Account acct = [Select Id from Account Where Name =: accountName Limit 1];
            
            if(acct.Id != null)
            {                
                List<Contact> userContactList = new List<Contact>();
                for(User usrList : [SELECT Id, FirstName, LastName, Email FROM User WHERE Id IN: Ids])
                {
                     userContactList.add(new Contact(FirstName = usrList.FirstName, LastName = usrList.LastName, 
                                                  Email = usrList.Email, AccountId = acct.Id, Internal_User__c = true));
                }
            
                insert userContactList;
            }
                       
                      
           
                
        }            
                
}