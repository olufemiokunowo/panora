public class AddMultipleMerchandiseHelper {
public static List<AddMultipleMerchandise.WrapperpaMerchandiseList> addNewRowToAccList(List<AddMultipleMerchandise.WrapperpaMerchandiseList> waAccObjList){
        AddMultipleMerchandise.WrapperpaMerchandiseList newRecord = new AddMultipleMerchandise.WrapperpaMerchandiseList();
        Merchandise__c newAccountRecord = new Merchandise__c();        
        newRecord.record = newAccountRecord;
        newRecord.index = waAccObjList.size();
        waAccObjList.add(newRecord);
        return waAccObjList;
    }
    
    
     public static List<AddMultipleMerchandise.WrapperpaMerchandiseList> removeRowToAccountList(Integer rowToRemove, List<AddMultipleMerchandise.WrapperpaMerchandiseList> waAccountList){
        waAccountList.remove(rowToRemove);
        return waAccountList;
    }
    
    public static void save(List<AddMultipleMerchandise.WrapperpaMerchandiseList> waAccList) {
        system.debug('==waAccList==>'+waAccList.size());
        List<Merchandise__c> accountRecordsToBeInserted = new List<Merchandise__c>();
        if(waAccList !=null && !waAccList.isEmpty()){
            for(AddMultipleMerchandise.WrapperpaMerchandiseList eachRecord : waAccList ){
                Merchandise__c accTemp = eachRecord.record;
                accountRecordsToBeInserted.add(accTemp);
               
            }
            system.debug('==accountRecordsToBeInserted==>'+accountRecordsToBeInserted.size());
            insert accountRecordsToBeInserted;
        }
    }
}