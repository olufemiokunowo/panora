public class recordSetVarController {
	public List<Contact> obj{get;set;}
    
    public recordSetVarController(ApexPages.StandardSetController controller) {
        obj = (List<Contact>) controller.getSelected();
    }
    

  /*public List<Contact> con;
  public void testemail()
  {
     //con = [select Id from Contact limit 250 ];
     for(Integer i=0;i<obj.size();i++)
     {
         contactids.add(obj[i].Id);
      }  
  }
*/
  public void SendEmail()
  {
      List<Id> contactids = new List<Id>();
      for(Integer i=0;i<obj.size();i++)
      {
         contactids.add(obj[i].Id);
      }  
 
      if (contactids.size() > 0)
      {        
      
       EmailTemplate template =  [SELECT Id, Name FROM EmailTemplate WHERE DeveloperName = 'Contact_Mass_Email' LIMIT 1];
       Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
       mail.setTargetObjectIds(contactids);
       mail.setTemplateId(template.Id);
       List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });
      
       Set<ID> targetIdsFailed = new Set<ID>();
	   for(Messaging.SendEmailResult rr:results){
       	if(!rr.IsSuccess())
          {
             // pull a list of unsuccessful ids
             Messaging.SendEmailError[] errArr = rr.getErrors();   
             targetIdsFailed.add(errArr[0].getTargetObjectId()); 
          }
    	}
      
      // pull all contactIds to a set
        Set<Id> contactsSet = new Set<Id>(contactids);
       // remove all the ones that failed, update survey date for those that passed
        Boolean result = contactsSet.removeAll(targetIdsFailed);
       
        List<Id> acctIds = new List<Id>();
        for(Contact cont : [Select AccountId, Name  from Contact where Id IN :contactsSet]){
        	acctIds.add(cont.AccountId);
        } 
         
         if (acctIds.size() > 0)
         {
             Account[] acct = [SELECT Id, Survey_Sent_Date__c FROM Account WHERE Id IN : acctIds];
             for(Integer i=0; i<acct.size(); i++)
             {
                 acct[i].Survey_Sent_Date__c = System.now();
             }
             
             update acct;
         }
      }
     
   
	}
      
  }