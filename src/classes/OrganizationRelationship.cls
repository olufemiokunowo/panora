public class OrganizationRelationship {
	/*
	public list<Account_Relationship__c> accountRelationshipParent {get; set;}
	public list<Account_Relationship__c> accountRelationshipChild {get; set;}
	*/
    public list<Account_Relationship__c> accountRelationshipMerge {get; set;}
    private Account acct;
       
    public OrganizationRelationship(ApexPages.StandardController controller) {
 		this.acct=(Account)controller.getRecord();
 	}

    public List<Account_Relationship__c> getOrganizationRelationship() { 
    	
    	list<Account_Relationship__c> accountRelationshipChild = new List<Account_Relationship__c> ();
    	list<Account_Relationship__c> accountRelationshipParent = new List<Account_Relationship__c> ();
    	accountRelationshipMerge = new List<Account_Relationship__c> ();

    	accountRelationshipParent = [SELECT ID, Name, 	Parent_Account__c, 	Child_Account__c, Parent_Account__r.Name, Child_Account__r.Name, Relationship_Action__c, Effective_Date__c 
    									FROM Account_Relationship__c WHERE Parent_Account__c = :ApexPages.currentPage().getParameters().get('id')]; 
    	   	

    	if (accountRelationshipParent.size() > 0)
    		{
    			accountRelationshipMerge.AddAll(accountRelationshipParent);
    		}
    	

    	
    	accountRelationshipChild = [SELECT ID, Name, 	Parent_Account__c, 	Child_Account__c, Parent_Account__r.Name, Child_Account__r.Name, Relationship_Action__c, Effective_Date__c 
    									FROM Account_Relationship__c WHERE Child_Account__c = :ApexPages.currentPage().getParameters().get('id')]; 

    	for (Account_Relationship__c acctRelationship : accountRelationshipChild)
    	{
    		if (acctRelationship.Relationship_Action__c == 'has as a member')
    			{
    				ID parentId = acctRelationship.Parent_Account__c;
                    ID childId = acctRelationship.Child_Account__c;
					//String parentName = Parent_Account__c.Name;
                    //String childName = Child_Account__c.Name;
                    //acctRelationship.Relationship_Action__c = 'is a member of';
                    //acctRelationship.Parent_Account__c = childId;
                    //acctRelationship.Child_Account__c = parentId;
                    //acctRelationship.Parent_Account__r.Name = childName;
                    //acctRelationship.Child_Account__r.Name = parentName;
    			} 
            accountRelationshipMerge.Add(acctRelationship);
            //accountRelationshipParent.Add(acctRelationship);
    	}
        return accountRelationshipMerge;
      
    } 

}