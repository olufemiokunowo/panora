// ****************************************************************************************************************
//
// *************************************************************************************************************** 

public class IdeaTriggerHandler {

    
    // ****************************************************************************************************************
    // OnAfterInsert - increment total # of ideas per campaign when new idea is added to a campaign
    // ****************************************************************************************************************
    public void OnBeforeInsert(List<Idea> theNewIdea_List)
    {
        Community comm = [SELECT Id, Name FROM Community WHERE Name = 'Security Zone'];
        
        for(Idea ideaItem: theNewIdea_List)
        { 
            if(ideaItem.SSI_Marking__c == 'Yes')
            {
                system.debug('Idea is SSI Marking');
                if(comm != null)
                {
                    system.debug('Security Zone ID is ' + comm.Id);
                    ideaItem.CommunityId  = comm.Id;
                    system.debug('New ideaItem zone ID is  ' + ideaItem.CommunityId);
                }
            }
        }
    }
    
  
    
}