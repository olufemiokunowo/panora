public class StripHtmlTagsFromText {

    string htmlText = ' <p>&nbsp;</p> ' +
	'<table bgcolor="#ffffff" border="1" cellspacing="0">' +
	'<caption>' + 
		'<font color="#000000" face="Calibri"><b>asdasdasdads</b></font></caption>' +
		'<thead>' +
		'<tr>' + 
		'<th bgcolor="#c0c0c0" bordercolor="#000000">' + 
		'<font color="#000000" face="Calibri" style="font-size: 11pt">asdasdasdasdas</font></th>' +
		'<th bgcolor="#c0c0c0" bordercolor="#000000">' +
		'<font color="#000000" face="Calibri" style="font-size: 11pt">asdasdasdas</font></th>' + 
		'<th bgcolor="#c0c0c0" bordercolor="#000000">' + 
		'<font color="#000000" face="Calibri" style="font-size: 11pt">asdasdsad</font></th>' +
		'<th bgcolor="#c0c0c0" bordercolor="#000000">' +
		'<font color="#000000" face="Calibri" style="font-size: 11pt">saasdasd</font></th>' +
		'</tr>' +
		'</thead>' + 
		'<tfoot> ' +
		'</tfoot> ' +
		'<tbody>' +
		'<tr valign="top">' +
		'<td bordercolor="#d0d7e5">' +
		'<font color="#000000" face="Calibri" style="font-size: 11pt">zxczczxcz</font></td>' +
		'<td bordercolor="#d0d7e5">' +
		'<font color="#000000" face="Calibri" style="font-size: 11pt">zsdasdasdasd/asdasdasdasdas</font></td>' +
		'<td bordercolor="#d0d7e5">' +
		'<font color="#000000" face="Calibri" style="font-size: 11pt">saskjdhaskjd lsdkasda askjdhash a a .. sfadasd e 8/24/16.</font></td>' +
		'<td align="right" bordercolor="#d0d7e5">' +
		'<font color="#000000" face="Calibri" style="font-size: 11pt">8/1/2015</font></td>' +
		'</tr>' +
		'</tbody>' +
		'</table>' +
		'<p>&nbsp;</p>';
    /*first replace all <BR> tags with \n to support new lines
    string result = html.replaceAll('<br/>', '\n');
    result.stripHtmlTags();
    result = result.replaceAll('<br />', '\n');
    
    //regular expression to match all HTML/XML tags
    string HTML_TAG_PATTERN = '<.*?>';
    
    // compile the pattern     
    pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
    
    // get your matcher instance
    matcher myMatcher = myPattern.matcher(result);
    
    //remove the tags     
    result = myMatcher.replaceAll('');
	*/
   public String invokeMethod()
   {
       return removeHTMLTags(htmlText);
   }
    
    public string removeHTMLTags(String textToStrip)
    {
        String result = textToStrip.stripHtmlTags();
        
        return result;
    }
    
}