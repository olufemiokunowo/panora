trigger AddRegion on Account (before insert, before update) {
          
        for (Account acct : Trigger.new) {
            String bState = acct.State_of_Incorporation__c;
            String orgType = acct.Organization_Type__c;
           
            if ((orgType == 'CFC' || orgType == 'RTFC' || orgType == 'NCSC Associate' || orgType == 'CFC/NCSC') && !String.isBlank(acct.AVP__c))
            {
                acct.OwnerId = acct.AVP__c;
            }

            if (orgType == 'CFC'){
                
                    Custom_Region__c region = Custom_Region__c.getInstance(orgType + '-' + bState);
                    acct.Region__c = region.District__c.intValue();
                    acct.Temp_Region__c = (acct.Alternate_Region__c == null) ?  string.valueof(integer.valueof(region.District__c)) : string.valueof(integer.valueof(acct.Alternate_Region__c));    
                                
            }
            else if (orgType == 'RTFC' || orgType == 'NCSC Associate'){
                   
                    Custom_Region__c region = Custom_Region__c.getInstance('RTFC' + '-' + bState);
                    acct.RTFC_Region__c = integer.valueof(region.District__c);
                    acct.Temp_Region__c = (acct.Alternate_Region__c == null) ? string.valueof(integer.valueof(region.District__c)) : string.valueof(integer.valueof(acct.Alternate_Region__c)); 
            }
            else if (orgType == 'CFC/NCSC')
            {
                    Custom_Region__c cfcRegion = Custom_Region__c.getInstance('CFC' + '-' + bState);
                    Custom_Region__c ncscRegion = Custom_Region__c.getInstance('RTFC' + '-' + bState);
                    acct.Region__c = integer.valueof(cfcRegion.District__c);
                    acct.RTFC_Region__c = integer.valueof(ncscRegion.District__c);
                    acct.Temp_Region__c = (acct.Alternate_Region__c == null) ? 
                                            string.valueof(integer.valueof(cfcRegion.District__c)) + '/' + string.valueof(integer.valueof(ncscRegion.District__c))
                                            :string.valueof(integer.valueof(acct.Alternate_Region__c)) ;
            }

    //update acct;
    }

}