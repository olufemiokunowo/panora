trigger HelloWorldPositionTrigger on Booking__c (before insert, before update) {
    List<Booking__c> bookings = Trigger.New;
    HelloWorldClass.helloWorld(bookings);    
}