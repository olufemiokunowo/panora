trigger IdeaTrigger on Idea (before insert) {

     //Instantiate the Handler
    IdeaTriggerHandler handler = new IdeaTriggerHandler();  
    
    if(Trigger.isInsert && Trigger.isBefore)
    {
        system.debug('IdeaTrigger -Before--Insert - 100'); 
        handler.OnBeforeInsert(trigger.new);
    }
}