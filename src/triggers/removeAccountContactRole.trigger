trigger removeAccountContactRole on Contact (after update) {
    Map<Id,String> contactIdMap = new Map<Id, String>();

    for (Contact cont : trigger.new)
    {          
          if (cont.Id != null){
              String currentConStatus = (cont.Contact_Status__c == true) ? 'True' : 'False';
              if(currentConStatus != cont.Contact_Status_History__c && cont.Contact_Status_History__c == 'True' && currentConStatus == 'False')
              {
                  // contact is inactivated
                  contactIdMap.put(cont.Id,cont.Contact_Status_History__c);
              }
              
          }
      }

      if(!contactIdMap.isEmpty()){

          AccountContactRole[] acctConRoles = [SELECT Id FROM AccountContactRole WHERE ContactId IN :contactIdMap.keyset()]; 
          try{
              if (acctConRoles.size() > 0)  
              	delete acctConRoles;
          }
          catch (DmlException e){
              String oops = 'Error';
          }

      }
}